﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;  
using System.Net.Sockets;  
using System.Threading;   


namespace Liran_Shmueli_EX20
{
    public partial class War_Game : Form
    {
        private TcpClient _Client;
        private NetworkStream _ClientStream;
        private IPEndPoint _ServerEndPoint;
        private bool Start_Press = false;
        public War_Game()
        {
            InitializeComponent();
            Generate_Cards();
        }
        public void Server_Connect()
        {
            //_Client = new TcpClient();
            //_ServerEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            //_Client.Connect(_ServerEndPoint);
            //_ClientStream = _Client.GetStream();
        }

        private void War_Game_Load(object sender, EventArgs e)
        {

        }

        public void Generate_Cards()
        {
       
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(12, 357);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::Liran_Shmueli_EX20.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(84, 122);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate(object sender1, EventArgs e1)
                {

                };
                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);
                // move to next line below
                nextLocation.X += currentPic.Size.Width + 20;

            }
        }
    }
}

